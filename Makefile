crawl = venv/bin/scrapy crawl

all: clean texas florida federal

test:
	python -m unittest discover

clean:
	rm -f inmatelocator/data/*.json

clearcache:
	rm -r .scrapy/httpcache

#
# State facility scrapers
#

texas:
	$(crawl) texas -o inmatelocator/data/texas.json

florida:
	$(crawl) florida -o inmatelocator/data/florida.json

federal:
	$(crawl) federal -o inmatelocator/data/federal.json
