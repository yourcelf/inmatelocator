import unittest
from utils import normalize_name

cases = {
    "WALTON C.I.": "walton ci",
    "GULF C.I.- ANNEX": "gulf ci annex"
}

class TestNormalize(unittest.TestCase):
    def test_normalize(self):
        for dirty, clean in cases.iteritems():
            self.assertEquals(normalize_name(dirty), clean)
