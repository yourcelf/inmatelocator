import unittest
from inmatelocator.stateparsers import utils

plain_normalize_cases = {
    "WALTON C.I.": "walton ci",
    "GULF C.I.- ANNEX": "gulf ci annex"
}

full_lookup_cases = {
    "florida": {
        "WALTON C.I.": "Walton Correctional Institution",
        "FLORIDA STATE PRISON": "Florida State Prison",
        "SUWANNEE C.I. ANNEX": "Suwannee Correctional Institution Annex",
        "SANTA ROSA C.I.": "Santa Rosa Correctional Institution",
        "NWFRC MAIN UNIT.": "Northwest Florida Reception Center",
        "CROSS CITY C.I.": "Cross City Correctional Institution",
        "TAYLOR C.I.": "Taylor Correctional Institution",
        "CENTURY C.I.": "Century Correctional Institution",
        "HARDEE C.I.": "Hardee Correctional Institution",
        "COLUMBIA ANNEX": "Columbia Correctional Institution Annex",
        "DESOTO ANNEX": "Desoto Annex",
        "HAMILTON C.I.": "Hamilton Correctional Institution",
    }
}

class TestNormalize(unittest.TestCase):
    def test_normalize(self):
        for dirty, clean in plain_normalize_cases.iteritems():
            self.assertEquals(utils.normalize_name(dirty), clean)


class TestFloridaLookup(unittest.TestCase):
    def test_florida(self):
        from inmatelocator.stateparsers.florida import facility_lookup

        for term, name in full_lookup_cases['florida'].iteritems():
            result = facility_lookup(term)
            self.assertFalse(result is None, "facility {} not found".format(term))
            self.assertEquals(result['organization'], name)
