# -*- coding: utf-8 -*-

# Scrapy settings for facilities project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'facilities'

SPIDER_MODULES = ['inmatelocator.facilities.spiders']
NEWSPIDER_MODULE = 'inmatelocator.facilities.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'facilities (+http://www.yourdomain.com)'

DOWNLOADER_MIDDLEWARES = {
  "scrapy.contrib.donwloadermiddleware.httpcache.HttpCacheMiddleware": None
}
HTTPCACHE_STORAGE = "scrapy.contrib.httpcache.FilesystemCacheStorage"
HTTPCACHE_POLICY = "scrapy.contrib.httpcache.DummyPolicy"
HTTPCACHE_ENABLED = True

ITEM_PIPELINES = {
        'inmatelocator.facilities.pipelines.FacilitiesPipeline': 300,
        'inmatelocator.facilities.pipelines.RemoveDuplicates': 500
}

from scrapy import log
LOG_LEVEL = log.INFO
