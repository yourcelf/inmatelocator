Facility data
=============

Problem: most of the prison person search facilities return a facility name or
code, and not the full address.

Solution: This scrapy scraper scrapes department of corrections facility lists
to yield complete address data.  Data is stored as JSON in ../data/.
