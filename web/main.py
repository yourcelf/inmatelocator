import json
import sys, os
DIR = os.path.join(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(DIR, ".."))
from flask import Flask, render_template, request, abort, make_response
from inmatelocator import stateparsers

app = Flask("Inmate Locator", template_folder=os.path.join(DIR, "templates"))
app.debug = True

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/search/")
def search():
    state = request.args["state"]
    if not state:
        res = make_response('{"error": "\'state\' query arg required"}', 400)
    elif state not in stateparsers.AVAILABLE_STATES:
        res = make_response('{"error": "state not available"}', 400)
    else:
        kwargs = {
            "first_name": request.args.get("first_name", ""),
            "last_name": request.args.get("last_name", ""),
            "number": request.args.get("number", ""),
        }
        results = getattr(stateparsers, state).search(**kwargs)
        res = make_response(json.dumps(results), 200)
    res.headers['Content-Type'] = "application/json"
    return res

if __name__ == '__main__':
    app.run()
